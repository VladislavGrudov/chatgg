const address = "localhost:8080";
function request(path,type){
    if (document.getElementById("loginName").value !== "" &&
        document.getElementById("loginPassword").value !== "") {

        var user = {
            username: document.getElementById("loginName").value,
            password: document.getElementById("loginPassword").value
        }

        var request = new XMLHttpRequest();

        request.open("POST", "http://" + address + "/users"+path, true);
        request.setRequestHeader("Content-Type","application/json");
        request.setRequestHeader("Accept","application/json");
        request.responseType="text";
        request.send(JSON.stringify(user));

        if(type==="reg"){
            request.onloadend = function () {
                document.getElementById("response").innerHTML = request.response;
            }
        }else{
            request.onloadend = function () {
                document.getElementById("response").innerHTML = request.response;
                var reqCheck=request.response;
                if (reqCheck === "true") {
                    localStorage.setItem('username',user.username);
                    window.location.href = "chat.html";
                }
            }
        }
    }
}

function Reg() {
    request("/addUser","reg");
}


function Login() {
    request("/checkUser","log");
}


