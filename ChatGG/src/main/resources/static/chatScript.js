const address = "localhost:8080";
var source=new EventSource("http://"+address+"/message/sseSubscribe?username="+localStorage.getItem('username'));

function updateUsers() {
    const requestSubs = new XMLHttpRequest();
    requestSubs.open("GET", "http://" + address + "/message/getAllSubscribes", true);
    requestSubs.send();
    requestSubs.onloadend = function () {

        const listActiveUser = JSON.parse(requestSubs.responseText);
        console.log(listActiveUser);
        for (let i = 0; i < listActiveUser.length; i++) {
            let elem = document.createElement("div");
            elem.id = listActiveUser[i];
            elem.innerHTML = listActiveUser[i];
            document.getElementById("users").append(elem);
        }
    }
}
function SendMessage() {

    const user = {
        username: localStorage.getItem('username'),
        message: document.getElementById("inp").value
    };

    const request = new XMLHttpRequest();
    request.open("POST", "http://" + address + "/message/sendMessage", true);
    request.setRequestHeader("Content-Type","application/json");
    request.setRequestHeader("Accept","application/json");
    request.send(JSON.stringify(user));
}

window.onload = function() {
    updateUsers();
    messageHistory();


}

window.onbeforeunload=function (){
    document.getElementById(localStorage.getItem('username')).remove();
}

function messageHistory(){
    const request = new XMLHttpRequest();
    request.open("GET", "http://" + address + "/message/messageHistory", true);
    request.send();

    request.onloadend = function () {
        const listMessages = JSON.parse(request.responseText);
        for (let i = 0; i < listMessages.length; i++) {
            let elem = document.createElement("div");
            elem.innerHTML = listMessages[i].username;
            elem.append(":::", listMessages[i].message);
            document.getElementById("container").append(elem);
        }

    }
}



source.addEventListener("message",function (event){
    let elem2 = document.createElement("div");
    elem2.innerHTML = JSON.parse(event.data).username;
    elem2.append(":::", JSON.parse(event.data).message);
    document.getElementById("container").append(elem2);
})

