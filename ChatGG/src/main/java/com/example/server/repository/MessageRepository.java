package com.example.server.repository;

import com.example.server.entity.MessageEntityNew;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<MessageEntityNew,Long> {
    List<MessageEntityNew> findAll();
}
