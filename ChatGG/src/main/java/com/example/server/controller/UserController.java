package com.example.server.controller;

import com.example.server.entity.UserEntity;
import com.example.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/addUser")
    public ResponseEntity registration(@RequestBody UserEntity user){
        try{
            userService.registration(user);
            return ResponseEntity.ok("Accept");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("User already exist");
        }

    }

    @PostMapping("/checkUser")
    public boolean checkUser(@RequestBody UserEntity user){
       return userService.checkUser(user);
    }
}
