package com.example.server.controller;

import com.example.server.entity.MessageEntityNew;
import com.example.server.model.MessageModel;
import com.example.server.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/message")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/sseSubscribe")
    public SseEmitter subscribe(@RequestParam String username){
       return  messageService.subscribe(username);
    }

    @GetMapping("/getAllSubscribes")
    public Set<String> getAllSubscribers(){
        return messageService.getAllSubscribers();
    }

    @PostMapping("/sendMessage")
    public void sendMessage(@RequestBody MessageEntityNew messageEntity){
        try {
            messageService.sendMessage(messageEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("/messageHistory")
    public List<MessageModel> getAllMessages(){
        return messageService.getAllMessages();
    }

}
