package com.example.server.service;

import com.example.server.entity.MessageEntityNew;
import com.example.server.model.MessageModel;
import com.example.server.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MessageService {
    private final Map<String, SseEmitter> emitters=new HashMap<>();
    private final MessageRepository repository;
    @Autowired
    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    public void sendMessage(MessageEntityNew message) throws IOException {
        for(SseEmitter emitter:emitters.values()){
            emitter.send(SseEmitter.event().name("message").data(MessageModel.toMessageModel(message)));
        }
        repository.save(message);
    }

    public SseEmitter subscribe(String username){
        SseEmitter sseEmitter = new SseEmitter(Long.MAX_VALUE);
        sseEmitter.onTimeout(()->{
            emitters.remove(username);
        });

        emitters.put(username,sseEmitter);
        return sseEmitter;
    }

    public List<MessageModel> getAllMessages(){
        return repository.findAll().stream().map(MessageModel::toMessageModel).collect(Collectors.toList());

    }


    public Set<String> getAllSubscribers() {
        return emitters.keySet();
    }
}
