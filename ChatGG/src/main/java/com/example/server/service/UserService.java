package com.example.server.service;

import com.example.server.entity.UserEntity;
import com.example.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserEntity registration(UserEntity user) throws Exception {
        if(userRepository.findByUsername(user.getUsername())!=null){
            throw new Exception();
        }
        return userRepository.save(user);
    }



    public boolean checkUser(UserEntity user) {
        UserEntity userEntity = userRepository.findByUsername(user.getUsername());
        return userEntity != null && user.getPassword().equals(userEntity.getPassword());
    }

}
