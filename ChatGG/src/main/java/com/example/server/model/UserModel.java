package com.example.server.model;

import com.example.server.entity.UserEntity;

public class UserModel {
    private Long id;
    private String username;

    public static UserModel toModel(UserEntity userEntity){
        UserModel model =new UserModel();
        model.setId(userEntity.getId());
        model.setUsername(userEntity.getUsername());
        return model;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
