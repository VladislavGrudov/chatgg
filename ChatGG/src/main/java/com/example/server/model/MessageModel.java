package com.example.server.model;

import com.example.server.entity.MessageEntityNew;

public class MessageModel {
    private String username;
    private String message;

    public static MessageModel toMessageModel(MessageEntityNew messageEntityNew){
        MessageModel messageModel=new MessageModel();
        messageModel.setMessage(messageEntityNew.getMessage());
        messageModel.setUsername(messageEntityNew.getUsername());
        return messageModel;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
